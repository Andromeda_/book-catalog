import firebase from "firebase/app";
import 'firebase/firestore';

const config = {
  apiKey: "AIzaSyDJdSk-xOtEVNRQl6I1QCryb5Jq4p7bH9A",
  authDomain: "book-catalog-a8578.firebaseapp.com",
  projectId: "book-catalog-a8578",
  storageBucket: "book-catalog-a8578.appspot.com",
  messagingSenderId: "54721178967",
  appId: "1:54721178967:web:232009a98956dd106a2212",
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(config);

const db = firebaseApp.firestore();
const booksCollection = db.collection("books");


export const createBook = book => {
  return booksCollection.add(book);
};

export const updateBook = (id, book) => {
  return booksCollection.doc(id).update(book);
};

export const deleteBook = async id => {
  return booksCollection.doc(id).delete();
};

export { db };
